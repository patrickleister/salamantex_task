-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 08. Dez 2018 um 17:24
-- Server-Version: 10.1.32-MariaDB
-- PHP-Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `salamantex_task`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `transactions`
--

CREATE TABLE `transactions` (
  `id` bigint(20) NOT NULL,
  `currency_amount` double NOT NULL,
  `currency_type` varchar(20) NOT NULL,
  `source_user_id` bigint(20) NOT NULL,
  `target_user_id` bigint(20) NOT NULL,
  `created_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `processed_timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `state` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Daten für Tabelle `transactions`
--

INSERT INTO `transactions` (`id`, `currency_amount`, `currency_type`, `source_user_id`, `target_user_id`, `created_timestamp`, `processed_timestamp`, `state`) VALUES
(4, 2.5, 'btc', 3, 1, '2018-11-29 07:26:55', '2018-12-08 15:40:12', 'confirmed'),
(5, 0.5, 'btc', 3, 1, '2018-12-08 15:41:30', '2018-12-08 15:41:40', 'confirmed'),
(6, 5, 'btc', 3, 1, '2018-12-08 15:42:13', '2018-12-08 15:42:23', 'cancelled'),
(7, 2, 'eth', 3, 1, '2018-12-08 15:45:35', '2018-12-08 15:45:50', 'confirmed'),
(8, 2.5, 'eth', 1, 3, '2018-12-08 16:10:22', '2018-12-08 16:10:23', 'cancelled'),
(9, 1.8, 'eth', 1, 3, '2018-12-08 16:14:41', '2018-12-08 16:14:42', 'confirmed'),
(10, 1.8, 'eth', 3, 1, '2018-12-08 16:16:46', '2018-12-08 16:16:47', 'confirmed');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `name` varchar(512) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `email` varchar(1000) NOT NULL,
  `btc_wallet_id` varchar(35) NOT NULL,
  `btc_wallet_balance` double NOT NULL,
  `eth_wallet_id` varchar(42) NOT NULL,
  `eth_wallet_balance` double NOT NULL,
  `max_transaction_amount` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

--
-- Daten für Tabelle `users`
--

INSERT INTO `users` (`id`, `name`, `description`, `email`, `btc_wallet_id`, `btc_wallet_balance`, `eth_wallet_id`, `eth_wallet_balance`, `max_transaction_amount`) VALUES
(1, 'Max Mustermann', 'Muster', 'maxmustermann@muster.com', '1BoatSLRHtKNngkdXEeobR76b53LETtpyT', 23, '0x4092678e4e78230f46a1534c0fbc8fa39780892b', 14.8, 2),
(2, 'Heidi Heidefrau', 'tante heidi', 'heidiheidefrau@example.com', '', 0, '', 0, 0),
(3, 'Max Test', 'maxi', 'max@test.com', '1F1tAaz5x1HUXrCNLbtMDqcw6o5GNn4xqX', 17, '0x343295B49522CFc38aF517c58eBB78565C42Ed95', 13, 3);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `source_user_id` (`source_user_id`),
  ADD KEY `target_user_id` (`target_user_id`);

--
-- Indizes für die Tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT für Tabelle `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `source_user_id` FOREIGN KEY (`source_user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `transactions_ibfk_1` FOREIGN KEY (`source_user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `transactions_ibfk_2` FOREIGN KEY (`target_user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
