package com.funny;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class RESTController {

    public List<Transaction> getTransactions() {
        List<Transaction> transactions = new ArrayList<Transaction>();

        try {
            URL url = new URL("http://localhost/salamantex_task/api/transaction/pending");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;

            while ((output = br.readLine()) != null) {
                transactions = parseTransactions(output);
            }

            conn.disconnect();

        }catch(Exception e){
            System.out.println(e.getMessage());
        }

        return transactions;
    }

    public List<Transaction> parseTransactions(String json) {
        json = json.replace("{\"status\":\"successful\",\"transactions\":[", "");
        json = json.replace("]}", "");

        String[] transactionStr = json.split("},");
        List<Transaction> transactions = new ArrayList<>();

        for (int i = 0; i < transactionStr.length; i++) {
            Transaction t = Transaction.getTransactionByJson_Preview(transactionStr[i]);
            transactions.add(t);
        }

        return transactions;
    }

    public Transaction getSingleTransaction(Long id){

        Transaction transaction = new Transaction();
        try {
            URL url = new URL("http://localhost/salamantex_task/api/transaction?id=" + id);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String json;

            while ((json = br.readLine()) != null) {
                json = json.replace("{\"status\":\"successful\",\"transaction\":{", "");
                json = json.replace("}", "");
                transaction = Transaction.getTransactionByJson(json);
            }

            conn.disconnect();

        }catch(Exception e){
            System.out.println(e.getMessage());
        }

        return transaction;
    }

    public User getUser(Long id) {

        User user = new User();

        try {
            URL url = new URL("http://localhost/salamantex_task/api/user?id=" + id);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String json;

            while ((json = br.readLine()) != null) {
                json = json.replace("{\"status\":\"successful\",\"user\":{", "");
                json = json.replace("}", "");
                user = User.getUserByJson(json);
            }

            conn.disconnect();

        }catch(Exception e){
            System.out.println(e.getMessage());
        }

        return user;
    }

    private void update(String url, String json){
        try {
            URL object = new URL(url);

            HttpURLConnection con = (HttpURLConnection) object.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept", "application/json");
            con.setRequestMethod("PUT");

            OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
            wr.write(json);
            wr.flush();

            //display what returns the POST request
            StringBuilder sb = new StringBuilder();
            int HttpResult = con.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(con.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
            }
        } catch(Exception e){}
    }

    public void updateTransaction(Transaction t) {

        String url ="http://localhost/salamantex_task/api/transaction?id=" + t.getId();

        String json =
                    "{" +
                        "\"currency_amount\" : \""+t.getCurrencyAmount()+"\", " +
                        "\"currency\" : \""+t.getCurrencyType()+"\", " +
                        "\"source_user_id\" : \""+t.getSourceUserId()+"\", " +
                        "\"target_user_id\" : \""+t.getTargetUserId()+"\", " +
                        "\"created_timestamp\" : \""+new Timestamp(t.getCreatedTimestamp().getTime())+"\", " +
                        "\"processed_timestamp\" : \""+new Timestamp(t.getProcessedTimestamp().getTime())+"\", " +
                        "\"state\" : \""+t.getState()+"\" " +
                    "}";

        this.update(url, json);
    }

    public void updateUser(User u) {

        String url = "http://localhost/salamantex_task/api/user?id=" + u.getId();

        String json =
                    "{" +
                        "\"name\" : \""+u.getName()+"\", " +
                        "\"description\" : \""+u.getDescription()+"\", " +
                        "\"email\" : \""+u.getEmail()+"\", " +
                        "\"btc_wallet_id\" : \""+u.getBtcWalletId()+"\", " +
                        "\"btc_wallet_balance\" : \""+u.getBtcWalletBalance()+"\", " +
                        "\"eth_wallet_id\" : \""+u.getEthWalletId()+"\", " +
                        "\"eth_wallet_balance\" : \""+u.getEthWalletBalance()+"\", " +
                        "\"max_transaction_amount\" : \""+u.getMaxTransactionAmount()+"\" " +
                    "}";

        this.update(url, json);
    }
}
