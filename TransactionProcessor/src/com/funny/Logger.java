package com.funny;

import java.time.LocalDateTime;

public class Logger {

    public static void Write(String s) {

        System.out.println(
                "[" + LocalDateTime.now().getYear() + "-" + LocalDateTime.now().getMonthValue() + "-" + LocalDateTime.now().getDayOfMonth() +
                        "|" + LocalDateTime.now().getHour() + ":" + LocalDateTime.now().getMinute() + ":" + LocalDateTime.now().getSecond() +
                "]: " + s
        );

    }
}
