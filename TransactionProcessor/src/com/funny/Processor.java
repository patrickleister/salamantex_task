package com.funny;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

public class Processor {

    public static void start() {

        boolean logger_msg = false;

        Logger.Write("Transaction-Processor started!");
        System.out.println("---------------------------------------------------------------------");
        Logger.Write("Wait for pending transactions ...");

        RESTController restController = new RESTController();

        while(true) {
            try { Thread.sleep(1000); } catch(Exception e) {}

            List<Transaction> transactions = restController.getTransactions();
            User sourceUser, targetUser;
            Boolean processTransaction = false;

            Collections.sort(transactions);

            for (Transaction t: transactions) {

                if (t.getId() != null) {

                    Logger.Write("Handle transaction with ID=" + t.getId() + ", Created-Timestamp=" + t.getCreatedTimestamp() + " ...");

                    t = restController.getSingleTransaction(t.getId());
                    sourceUser = restController.getUser(t.getSourceUserId());
                    targetUser = restController.getUser(t.getTargetUserId());


                    if (t.getCurrencyType().toLowerCase().equals("btc")) {
                        if (sourceUser.getBtcWalletId() == null || targetUser.getBtcWalletId() == null || t.getCurrencyAmount() > sourceUser.getBtcWalletBalance()) {
                            Logger.Write("Transaction is not valid - BTC-Parameters are wrong!");
                            processTransaction = false;
                        } else {
                            processTransaction = true;
                        }
                    } else if (t.getCurrencyType().toLowerCase().equals("eth")) {
                        if (sourceUser.getEthWalletId() == null || targetUser.getEthWalletId() == null || t.getCurrencyAmount() > sourceUser.getEthWalletBalance()) {
                            Logger.Write("Transaction is not valid - ETH-Parameters are wrong!");
                            processTransaction = false;
                        } else {
                            processTransaction = true;
                        }
                    } else {
                        processTransaction = false;
                    }
                    if(t.getCurrencyAmount() > sourceUser.getMaxTransactionAmount()) { processTransaction = false; }

                    if (processTransaction) {

                        if (t.getCurrencyType().toLowerCase().equals("btc")) {
                            Logger.Write("BTC-Transaction is valid - process transaction!");
                            Logger.Write("Transfer " + t.getCurrencyAmount() + " BTC from the Source-User with ID=" + sourceUser.getId() + " to the Target-User with ID=" + targetUser.getId() + "...");
                            sourceUser.setBtcWalletBalance((sourceUser.getBtcWalletBalance() - t.getCurrencyAmount()));         //check if user has a required wallet!!!!
                            targetUser.setBtcWalletBalance((targetUser.getBtcWalletBalance() + t.getCurrencyAmount()));
                            Logger.Write("Transaction confirmed");
                            t.setState("confirmed");

                        } else if (t.getCurrencyType().toLowerCase().equals("eth")) {
                            Logger.Write("ETH-Transaction is valid - process transaction!");
                            Logger.Write("Transfer " + t.getCurrencyAmount() + " ETH from the Source-User with ID=" + sourceUser.getId() + " to the Target-User with ID=" + targetUser.getId() + "...");
                            sourceUser.setEthWalletBalance((sourceUser.getEthWalletBalance() - t.getCurrencyAmount()));
                            targetUser.setEthWalletBalance((targetUser.getEthWalletBalance() + t.getCurrencyAmount()));
                            Logger.Write("Transaction confirmed!");
                            t.setState("confirmed");

                        }
                    } else {
                        Logger.Write("Transaction cancelled!");
                        t.setState("cancelled");
                    }

                    Logger.Write("Set Processed-Timestamp ...");
                    LocalDateTime l = LocalDateTime.now();
                    SimpleDateFormat sdfSource = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    try {
                        t.setProcessedTimestamp(sdfSource.parse(l.getYear() + "-" + l.now().getMonthValue() + "-" + l.getDayOfMonth() + " " + l.getHour() + ":" + l.getMinute() + ":" + l.getSecond()));
                    } catch (Exception e) {
                        Logger.Write("Error when parsing processed timestamp!");
                    }

                    processTransaction = false;

                    Logger.Write("Send update to database ...");
                    restController.updateTransaction(t);
                    restController.updateUser(sourceUser);
                    restController.updateUser(targetUser);
                    Logger.Write("Transaction completed!");
                    System.out.println("---------------------------------------------------------------------");
                    logger_msg = true;
                }
            }

            if(logger_msg){
                Logger.Write("Wait for pending transactions ...");
                logger_msg = false;
            }
        }
    }

}
