package com.funny;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {

    private Long id;
    private String name;
    private String description;
    private String email;
    private String btcWalletId;
    private Double btcWalletBalance;
    private String ethWalletId;
    private Double ethWalletBalance;
    private Double maxTransactionAmount;

    public static User getUserByJson(String json) {

        User u = new User();

        json = json.replace("{", "");
        json = json.replace("}", "");
        json = json.replace("\"", "");

        String[] attributes = json.split(",");
        try {
            u.setId(Long.parseLong(attributes[0].split(":")[1]));
            u.setName(attributes[1].split(":")[1]);
            u.setDescription(attributes[2].split(":")[1]);
            u.setEmail(attributes[3].split(":")[1]);
            if(attributes[4].split(":").length > 1)
                u.setBtcWalletId(attributes[4].split(":")[1]);
            if(attributes[5].split(":").length > 1)
                u.setBtcWalletBalance(Double.parseDouble(attributes[5].split(":")[1]));
            if(attributes[6].split(":").length > 1)
                u.setEthWalletId(attributes[6].split(":")[1]);
            if(attributes[7].split(":").length > 1)
                u.setEthWalletBalance(Double.parseDouble(attributes[7].split(":")[1]));
            if(attributes[8].split(":").length > 1 )
                u.setMaxTransactionAmount(Double.parseDouble(attributes[8].split(":")[1]));
        }catch (Exception e){Logger.Write(e.getMessage());}

        return u;
    }
}
