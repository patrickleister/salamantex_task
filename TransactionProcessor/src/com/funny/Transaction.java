package com.funny;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Transaction implements Comparable<Transaction> {

    private Long id;
    private Double currencyAmount;
    private String currencyType;
    private Long sourceUserId;
    private Long targetUserId;
    private Date createdTimestamp;
    private Date processedTimestamp;
    private String state;

    public static Transaction getTransactionByJson_Preview(String json) {

        Transaction t = new Transaction();

        json = json.replace("{", "");
        json = json.replace("}", "");
        json = json.replace("\"", "");

        String[] attributes = json.split(",");
        SimpleDateFormat sdfSource = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {
            t.setId(Long.parseLong(attributes[0].split(":")[1]));
            t.setCurrencyType(attributes[1].split(":")[1]);
            t.setCreatedTimestamp(sdfSource.parse(attributes[2].split(":")[1] + ":" + attributes[2].split(":")[2] + ":" + attributes[2].split(":")[3]));
        }catch (Exception e){}

        return t;
    }

    public static Transaction getTransactionByJson(String json) {

        Transaction t = new Transaction();

        json = json.replace("{", "");
        json = json.replace("}", "");
        json = json.replace("\"", "");

        String[] attributes = json.split(",");
        SimpleDateFormat sdfSource = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {
            t.setId(Long.parseLong(attributes[0].split(":")[1]));
            t.setCurrencyType(attributes[1].split(":")[1]);
            t.setCurrencyAmount(Double.parseDouble(attributes[2].split(":")[1]));
            t.setSourceUserId(Long.parseLong(attributes[3].split(":")[1]));
            t.setTargetUserId(Long.parseLong(attributes[4].split(":")[1]));
            t.setCreatedTimestamp(sdfSource.parse(attributes[5].split(":")[1] + ":" + attributes[5].split(":")[2] + ":" + attributes[5].split(":")[3]));
            t.setProcessedTimestamp(sdfSource.parse(attributes[6].split(":")[1] + ":" + attributes[6].split(":")[2] + ":" + attributes[6].split(":")[3]));
            t.setState(attributes[7].split(":")[1]);
        }catch (Exception e){}

        return t;
    }

    @Override
    public int compareTo(Transaction t) {
        return this.getCreatedTimestamp().compareTo(t.getCreatedTimestamp());
    }
}
