<?php

require_once __DIR__ .'/vendor/autoload.php';

use routes\Routing;

Routing::route();
