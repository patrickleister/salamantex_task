<?php

namespace controller;
require_once __DIR__ . '/../../vendor/autoload.php';

use PHPUnit\Runner\Exception;
use Klein\Request;
use Klein\Response;
use model\User;
use model\http\HttpStatus;
use model\http\HttpMethod;
use model\http\HttpResponse;
use domain\UserRepository;
use domain\TransactionRepository;

class UserController {

    public static function get(Request $request, Response $response) {
        include __DIR__ . '/../config.php';
        
        if($request->paramsGet()['id'] == null) {
            $users = UserRepository::findAll();

            $json = '[';
            for($i = 0; $i < sizeof($users); $i++){
                $json .= $users[$i]->shortJson();
                if( ($i + 1) < sizeof($users)) {
                    $json .= ',';
                }
            }
            $json .= ']';
            
            $response_json = json_encode(array('status' => 'successful', 'users' => json_decode($json)));
            return HttpResponse::getMessage($response, HttpStatus::OK, $response_json);
        }

        $user = UserRepository::find($request->paramsGet()['id']);
        
        if($user->getEmail() == null){
            $response_json = json_encode(array('status' => 'failed', 'msg' => "There is no user with this ID!"));
            return HttpResponse::getMessage($response, HttpStatus::NOT_FOUND, $response_json);
        }

        $response_json = json_encode(array('status' => 'successful', 'user' => json_decode($user->json())));
        return HttpResponse::getMessage($response, HttpStatus::OK, $response_json);
    }

    public static function create(Request $request, Response $response) {

        include __DIR__ . '/../config.php';

        $data = json_decode($request->body(), true);
        $user = User::normal($data);
        
        if($data['name'] == "" || $data['description'] == "" || $data['email'] == "" ){
            $response_json = json_encode(array('status' => 'failed', 'msg' => "Invalid parameters!"));
            return HttpResponse::getMessage($response, HttpStatus::BAD_REQUEST, $response_json);
        }

        if(UserRepository::findByEmail($user->getEmail())->getEmail() == null){

            $id = UserRepository::create($user);
           
            $response_json = json_encode(array('status' => 'successful', 'created_id' => $id));
            return HttpResponse::getMessage($response, HttpStatus::CREATED, $response_json);
        }

        $response_json = json_encode(array('status' => 'failed', 'msg' => "There is already an user with this E-Mail!"));
        return HttpResponse::getMessage($response, HttpStatus::NOT_ACCEPTABLE, $response_json);
    }

    public static function addTransactionAccount(Request $request, Response $response){
        
        include __DIR__ . '/../config.php';

        $data = json_decode($request->body(), true);
        $user = UserRepository::find($request->paramsNamed()['id']);
        
        if($data['wallet_id'] == "" || $data['wallet_balance'] == "" ||  $data['currency'] == "" || floatval($data['wallet_balance']) > 1000000000 || !is_numeric($data['wallet_balance']) || $data['max_transaction_amount'] == "" || !is_numeric($data['max_transaction_amount'])){
            $response_json = json_encode(array('status' => 'failed', 'msg' => "Invalid parameters!"));
            return HttpResponse::getMessage($response, HttpStatus::BAD_REQUEST, $response_json);
        }
        if($user->getEmail() == null){
            $response_json = json_encode(array('status' => 'failed', 'msg' => "There is no user with this ID!"));
            return HttpResponse::getMessage($response, HttpStatus::NOT_FOUND, $response_json);
        }

        if(strtolower($data["currency"]) == "btc"){
            if(strlen($data["wallet_id"]) > 35) {
                $response_json = json_encode(array('status' => 'failed', 'msg' => "Wallet-Id is not valid for the currency!"));
                return HttpResponse::getMessage($response, HttpStatus::BAD_REQUEST, $response_json);    
            } 
            else {
                $user->setBtcWalletId($data['wallet_id']);
                $user->setBtcWalletBalance($data['wallet_balance']);
            }
        }
        else if(strtolower($data["currency"]) == "eth") {
            if(strlen($data["wallet_id"]) > 42) {
                $response_json = json_encode(array('status' => 'failed', 'msg' => "Wallet-Id is not valid for the currency!"));
                return HttpResponse::getMessage($response, HttpStatus::BAD_REQUEST, $response_json);       
            } 
            else {
                $user->setEthWalletId($data['wallet_id']);
                $user->setEthWalletBalance($data['wallet_balance']);
            }
        }
        else {
            $response_json = json_encode(array('status' => 'failed', 'msg' => "Currency-Type not valid!"));
            return HttpResponse::getMessage($response, HttpStatus::BAD_REQUEST, $response_json);
        }
        
        $user->setMaxTransactionAmount($data['max_transaction_amount']);

        UserRepository::update($request->paramsNamed()['id'], $user);

        $response_json = json_encode(array('status' => 'successful', 'msg' => "currency account added!"));
        return HttpResponse::getMessage($response, HttpStatus::OK, $response_json);
    }

    public static function getTransactionHistoryByUser(Request $request, Response $response) {
        include __DIR__ . '/../config.php';

        $id = $request->paramsNamed()['id'];
        
        $user = UserRepository::find($id);
        if($user->getEmail() ==  null){
            $response_json = json_encode(array('status' => 'failed', 'msg' => "There is no user with this ID!"));
            return HttpResponse::getMessage($response, HttpStatus::NOT_FOUND, $response_json);
        }
        
        $json = ' [';
        
        $transactions_by_source_user = TransactionRepository::findBySourceUserId($id);
        $transactions_by_target_user = TransactionRepository::findByTargetUserId($id);

        for($i = 0; $i < sizeof($transactions_by_source_user); $i++){
            $json .=    '{ 
                            "id" : "'.$transactions_by_source_user[$i]->getId().'",
                            "currency" : "'.$transactions_by_source_user[$i]->getCurrencyType().'",
                            "state" : "'.$transactions_by_source_user[$i]->getState().'"
                        }';
            if( sizeof($transactions_by_target_user) > 0){
                $json .= ',';
            }
            else if(($i+1) < sizeof($transactions_by_source_user)) {
                $json .= ',';
            }
        }

        for($i = 0; $i < sizeof($transactions_by_target_user); $i++){
            $json .=    '{ 
                            "id" : "'.$transactions_by_target_user[$i]->getId().'",
                            "currency" : "'.$transactions_by_target_user[$i]->getCurrencyType().'",
                            "state" : "'.$transactions_by_target_user[$i]->getState().'"
                        }';
            if( ($i+1) < sizeof($transactions_by_target_user)){
                $json .= ',';
            }
        }

        $json .= ']'; 

        $response_json = json_encode(array('status' => 'successful', 'transaction_history' => json_decode($json)));
        return HttpResponse::getMessage($response, HttpStatus::OK, $response_json);
    }

    public static function update(Request $request, Response $response) {
        include __DIR__ . '/../config.php';

        $data = json_decode($request->body(), true);
        $user = UserRepository::find($request->paramsGet()['id']);
        
        if(floatval($data['btc_wallet_balance']) > 1000000000 || floatval($data['eth_wallet_balance']) > 1000000000){
            $response_json = json_encode(array('status' => 'failed', 'msg' => "Invalid parameters!"));
            return HttpResponse::getMessage($response, HttpStatus::BAD_REQUEST, $response_json);
        }

        if($user->getEmail() == null){
            $response_json = json_encode(array('status' => 'failed', 'msg' => "There is no user with this ID!"));
            return HttpResponse::getMessage($response, HttpStatus::NOT_FOUND, $response_json);
        }

        if(strlen($data["btc_wallet_id"]) > 35) {
            $response_json = json_encode(array('status' => 'failed', 'msg' => "BTC-Wallet-Id is not valid!"));
            return HttpResponse::getMessage($response, HttpStatus::BAD_REQUEST, $response_json);    
        } 
        else {
            $user->setBtcWalletId($data['btc_wallet_id']);
            $user->setBtcWalletBalance($data['btc_wallet_balance']);
        }
    
        if(strlen($data["eth_wallet_id"]) > 42) {
            $response_json = json_encode(array('status' => 'failed', 'msg' => "ETH-Wallet-Id is not valid!"));
            return HttpResponse::getMessage($response, HttpStatus::BAD_REQUEST, $response_json);       
        } 
        else {
            $user->setEthWalletId($data['eth_wallet_id']);
            $user->setEthWalletBalance($data['eth_wallet_balance']);
        }
        
        $user->setMaxTransactionAmount($data["max_transaction_amount"]);
    
        UserRepository::update($request->paramsGet()['id'], $user);

        $response_json = json_encode(array('status' => 'successful', 'msg' => "User successfully updated!"));
        return HttpResponse::getMessage($response, HttpStatus::OK, $response_json);

    }

}