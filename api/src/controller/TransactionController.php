<?php

namespace controller;
require_once __DIR__ . '/../../vendor/autoload.php';

use PHPUnit\Runner\Exception;
use Klein\Request;
use Klein\Response;
use model\Transaction;
use model\User;
use model\http\HttpStatus;
use model\http\HttpMethod;
use model\http\HttpResponse;
use domain\TransactionRepository;
use domain\UserRepository;

class TransactionController {

    public static function getTransaction(Request $request, Response $response ) {
        include __DIR__ . '/../config.php';
        
        $id = $request->paramsGet()['id'];

        $transaction = TransactionRepository::find($id);
     
        if($transaction->getId() == null) {
            $response_json = json_encode(array('status' => 'failed', 'msg' => "There is no transaction with this ID!"));
            return HttpResponse::getMessage($response, HttpStatus::NOT_FOUND, $response_json);
        }

        $json = '
        {
            "id" : "'.$transaction->getId().'",
            "currency" : "'.$transaction->getCurrencyType().'",
            "currency_amount" : "'.$transaction->getCurrencyAmount().'",
            "source_user_id" : "'.$transaction->getSourceUserId().'",
            "target_user_id" : "'.$transaction->getTargetUserId().'",
            "created_timestamp" : "'.$transaction->getCreatedTimestamp().'",
            "processed_timestamp" : "'.$transaction->getProcessedTimestamp().'",
            "state": "'.$transaction->getState().'"
        }
        ';

        $response_json = json_encode(array('status' => 'successful', 'transaction' => json_decode($json)));
        return HttpResponse::getMessage($response, HttpStatus::OK, $response_json);
    }

    public static function createTransaction(Request $request, Response $response) {
        include __DIR__ . '/../config.php';

        $data = json_decode($request->body(), true);

        //region check parameters
        if($data['currency'] == "" || $data['currency_amount'] == "" || $data['source_user_id'] == "" || $data['target_user_id'] == "" ){
            $response_json = json_encode(array('status' => 'failed', 'msg' => "Invalid parameters!"));
            return HttpResponse::getMessage($response, HttpStatus::BAD_REQUEST, $response_json);
        }
        //endregion

        //region check currency-type
        if(strtolower($data["currency"]) != "btc" && strtolower($data["currency"]) != "eth"){
            $response_json = json_encode(array('status' => 'failed', 'msg' => "Currency-Type not valid!"));
            return HttpResponse::getMessage($response, HttpStatus::BAD_REQUEST, $response_json);
        }
        //endregion

        //region check user-ids
        
        if($data["source_user_id"] == $data["target_user_id"]){
            $response_json = json_encode(array('status' => 'failed', 'msg' => "Source- and target-user are the same!"));
            return HttpResponse::getMessage($response, HttpStatus::BAD_REQUEST, $response_json);
        }
        
        $source_user = UserRepository::find($data['source_user_id']);
        if($source_user->getEmail() ==  null){
            $response_json = json_encode(array('status' => 'failed', 'msg' => "The source-user with this ID does not exist!"));
            return HttpResponse::getMessage($response, HttpStatus::NOT_FOUND, $response_json);
        }

        $target_user = UserRepository::find($data['target_user_id']);
        if($target_user->getEmail() ==  null){
            $response_json = json_encode(array('status' => 'failed', 'msg' => "The target-user with this ID does not exist!"));
            return HttpResponse::getMessage($response, HttpStatus::NOT_FOUND, $response_json);
        }

        if(strtolower($data["currency"]) == "btc"){
            $data["currency_type"] = "btc";
            if( $data["currency_amount"] > $source_user->getBtcWalletBalance()){
                $response_json = json_encode(array('status' => 'failed', 'msg' => "BTC-Balance of the source-user is too low!"));
                return HttpResponse::getMessage($response, HttpStatus::BAD_REQUEST, $response_json);
            }
        }
        else if(strtolower($data["currency"]) == "eth"){
            $data["currency_type"] = "eth";
            if( $data["currency_amount"] > $source_user->getEthWalletBalance()){
                $response_json = json_encode(array('status' => 'failed', 'msg' => "ETH-Balance of the source-user is too low!"));
                return HttpResponse::getMessage($response, HttpStatus::BAD_REQUEST, $response_json);
            }
        }
        //endregion

        $data["state"] = "pending";
        $transaction = Transaction::normal($data);
        $id = TransactionRepository::create($transaction);

        $response_json = json_encode(array('status' => 'successful', 'transaction_id' => $id));
        return HttpResponse::getMessage($response, HttpStatus::CREATED, $response_json);       
    }

    public static function getTransactionState(Request $request, Response $response) {
        include __DIR__ . '/../config.php';

        $id = $request->paramsNamed()['id'];
        $transaction = TransactionRepository::find($id);
        
        if($transaction->getCurrencyAmount() == "") {
            $response_json = json_encode(array('status' => 'failed', 'msg' => "There is no transaction with this ID!"));
            return HttpResponse::getMessage($response, HttpStatus::NOT_FOUND, $response_json); 
        }
        
        $response_json = json_encode(array('status' => 'successful', 'transaction_state' => $transaction->getState()));
        return HttpResponse::getMessage($response, HttpStatus::OK, $response_json);       
    }

    public static function getPendingTransactions(Request $request, Response $response) {
        include __DIR__ . '/../config.php';

        $transactions = TransactionRepository::findByState("pending");

        $json = '[';

        for($i = 0; $i < sizeof($transactions); $i++) {
            $json .= '{
                "id" : "'.$transactions[$i]->getId().'",
                "currency" : "'.$transactions[$i]->getCurrencyType().'",
                "created_timestamp" : "'.$transactions[$i]->getCreatedTimeStamp().'"
            }';

            if( ($i+1) < sizeof($transactions)){
                $json .= ',';
            }
        }

        $json .= ']'; 

        $response_json = json_encode(array('status' => 'successful', 'transactions' => json_decode($json)));
        return HttpResponse::getMessage($response, HttpStatus::OK, $response_json);      
    }

    public static function update(Request $request, Response $response){

        $id = $request->paramsGet()['id'];
        $data = json_decode($request->body(), true);
        
        if($id == null){
            $response_json = json_encode(array('status' => 'failed', 'msg' => "No ID!"));
            return HttpResponse::getMessage($response, HttpStatus::BAD_REQUEST, $response_json);
        }
        
        $t = TransactionRepository::find($id);
        if($t->getState() == ""){
            $response_json = json_encode(array('status' => 'failed', 'msg' => "There is no transaction with this ID!"));
            return HttpResponse::getMessage($response, HttpStatus::NOT_FOUND, $response_json);  
        }
 
        /*
        if($data['currency'] == "" || $data['currency_amount'] == "" || $data['source_user_id'] == "" || $data['target_user_id'] == "" ){
            $response_json = json_encode(array('status' => 'failed', 'msg' => "Invalid parameters!"));
            return HttpResponse::getMessage($response, HttpStatus::BAD_REQUEST, $response_json);
        }
        */
        
        $res = [
            "id" => $id,
            "currency_amount" => $data["currency_amount"],
            "currency_type" => $data["currency"],
            "source_user_id" => $data["source_user_id"],
            "target_user_id" => $data["target_user_id"],
            "created_timestamp" => $data["created_timestamp"],
            "processed_timestamp" => $data["processed_timestamp"],
            "state" => $data["state"]
        ];
        TransactionRepository::update($id, Transaction::allParam($res));

        $response_json = json_encode(array('status' => 'successful', 'msg' => 'Transaction successfully updated!'));
        return HttpResponse::getMessage($response, HttpStatus::OK, $response_json);       
        
    }
}