<?php

require_once __DIR__ . '/../vendor/autoload.php';

// Allgemeines
$application_name = "Salamantex-Task";
$website_base_url = "localhost";                                    // Website-Url

// Datenbank
$db_path = "mysql:host=localhost;dbname=salamantex_task";           // Datenbank-Pfad
$db_user = "root";                                                  // Datenbank-Benutzer
$db_password = "";                                                  // Datenbank-Passwort
