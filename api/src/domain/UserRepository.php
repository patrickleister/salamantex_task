<?php

namespace domain;
require_once __DIR__ . '/../../vendor/autoload.php';

use model\User;
use domain\DBConnection;

class UserRepository {

    const FIND_ALL = "SELECT * FROM USERS";
    const FIND = "SELECT * FROM USERS WHERE id=:id";
    const CREATE = "INSERT INTO USERS (NAME, DESCRIPTION, EMAIL) VALUES (:name, :description, :email)";
    const FIND_BY_EMAIL = "SELECT * FROM USERS WHERE EMAIL=:email";
    const UPDATE = "UPDATE USERS SET NAME=:name, DESCRIPTION=:description, EMAIL=:email, BTC_WALLET_ID=:btc_wallet_id, BTC_WALLET_BALANCE=:btc_wallet_balance, ETH_WALLET_ID=:eth_wallet_id, ETH_WALLET_BALANCE=:eth_wallet_balance, MAX_TRANSACTION_AMOUNT=:max_transaction_amount WHERE ID=:id";

    public static function findAll() {
        $connection = DBConnection::getInstance()->getConnection();
        
        $stmt = $connection->prepare(self::FIND_ALL);     
        $stmt->execute();
        $result = $stmt->fetchAll();
        $users = array();
        
        foreach($result as $r){
            $users[] = User::allParam($r);
        }
        
        return $users;         
    }

    public static function find($id){
        $connection = DBConnection::getInstance()->getConnection();
        
        $stmt = $connection->prepare(self::FIND);     
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        $result = $stmt->fetch();
        
        return User::allParam($result); 
    }

    public static function create(User $user) {
        $connection = DBConnection::getInstance()->getConnection();

        $name = $user->getName();
        $description= $user->getDescription();
        $email= $user->getEmail();

        $stmt = $connection->prepare(self::CREATE);
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':description', $description);        
        $stmt->bindParam(':email', $email);
        $stmt->execute();
        
        return $connection->lastInsertId();
    }

    public static function findByEmail($email){
        $connection = DBConnection::getInstance()->getConnection();#
        
        $stmt = $connection->prepare(self::FIND_BY_EMAIL);     
        $stmt->bindParam(':email', $email);
        $stmt->execute();
        $result = $stmt->fetch();
        
        return User::allParam($result); 
    }

    public static function update($id, User $user){
        $connection = DBConnection::getInstance()->getConnection();

        $name = $user->getName();
        $description= $user->getDescription();
        $email= $user->getEmail();
        $btcWalletId = $user->getBtcWalletId();
        $btcWalletBalance= $user->getBtcWalletBalance();
        $ethWalletId= $user->getEthWalletId();
        $ethWalletBalance = $user->getEthWalletBalance();
        $maxTransactionAmount= $user->getMaxTransactionAmount();

        $stmt = $connection->prepare(self::UPDATE);
        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':description', $description);        
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':btc_wallet_id', $btcWalletId);
        $stmt->bindParam(':btc_wallet_balance', $btcWalletBalance);        
        $stmt->bindParam(':eth_wallet_id', $ethWalletId);
        $stmt->bindParam(':eth_wallet_balance', $ethWalletBalance);
        $stmt->bindParam(':max_transaction_amount', $maxTransactionAmount);

        $stmt->execute();
    }
}