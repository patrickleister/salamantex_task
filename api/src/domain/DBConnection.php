<?php 

namespace domain;
require_once __DIR__ . '/../../vendor/autoload.php';

use \PDO;

class DBConnection{

    private $conn = null;
    private static $_instance;

    public static function getInstance(){

        if(self::$_instance === null){
            self::$_instance = new DBConnection();
            self::$_instance->init();
        }

        return DBConnection::$_instance;
    }

    private function __construct() {
        //$this->conn = new PDO(DBConnection::MYSQL_DB_URI, DBConnection::USER_NAME_DB, DBConnection::PASSWORD_DB);
        //$conn->SetAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    private function init(){
        include __DIR__ . '/../config.php';
        self::$_instance->conn = new PDO($db_path, $db_user, $db_password);
    }

    public function getConnection(){
        return self::$_instance->conn;
    }

    public function closeConnection(){
        return self::$_instance->conn = null;
    }

}