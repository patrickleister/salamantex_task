<?php

namespace domain;
require_once __DIR__ . '/../../vendor/autoload.php';

use model\Transaction;
use domain\DBConnection;

class TransactionRepository {

    const CREATE = "INSERT INTO TRANSACTIONS(CURRENCY_AMOUNT, CURRENCY_TYPE, SOURCE_USER_ID, TARGET_USER_ID, STATE) VALUES(:currency_amount, :currency_type, :source_user_id, :target_user_id, :state)";
    const FIND = "SELECT * FROM TRANSACTIONS WHERE ID=:id";
    const FIND_BY_STATE = "SELECT * FROM TRANSACTIONS WHERE STATE=:state";
    const FIND_BY_SOURCE_USER_ID = "SELECT * FROM TRANSACTIONS WHERE SOURCE_USER_ID=:source_user_id";
    const FIND_BY_TARGET_USER_ID =  "SELECT * FROM TRANSACTIONS WHERE TARGET_USER_ID=:target_user_id";
    const UPDATE = "UPDATE TRANSACTIONS SET CURRENCY_AMOUNT=:currency_amount, CURRENCY_TYPE=:currency_type, SOURCE_USER_ID=:source_user_id, TARGET_USER_ID=:target_user_id, CREATED_TIMESTAMP=:created_timestamp, PROCESSED_TIMESTAMP=:processed_timestamp, STATE=:state WHERE ID=:id";

    public static function create(Transaction $transaction) {
        $connection = DBConnection::getInstance()->getConnection();
        
        $currency_amount = $transaction->getCurrencyAmount();
        $currency_type = $transaction->getCurrencyType();
        $source_user_id = $transaction->getSourceUserId();
        $target_user_id = $transaction->getTargetUserId();
        $state = $transaction->getState();

        $stmt = $connection->prepare(self::CREATE);
        $stmt->bindParam(":currency_amount", $currency_amount);
        $stmt->bindParam(":currency_type", $currency_type);
        $stmt->bindParam(":source_user_id", $source_user_id);
        $stmt->bindParam(":target_user_id", $target_user_id);
        $stmt->bindParam(":state", $state);
        $stmt->execute();

        return $connection->lastInsertId();
    }

    public static function find($id) {
        $connection = DBConnection::getInstance()->getConnection();
        
        $stmt = $connection->prepare(self::FIND);     
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        $result = $stmt->fetch();

        return Transaction::allParam($result);
    }

    public static function findByState($state) {
        $connection = DBConnection::getInstance()->getConnection();

        $stmt = $connection->prepare(self::FIND_BY_STATE);
        $stmt->bindParam(':state', $state);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $transactions = array();
        
        foreach($result as $r){
            $transactions[] = Transaction::allParam($r);
        }

        return $transactions;
    }

    public static function findBySourceUserId($id) {
        $connection = DBConnection::getInstance()->getConnection();
        
        $stmt = $connection->prepare(self::FIND_BY_SOURCE_USER_ID);     
        $stmt->bindParam(':source_user_id', $id);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $transactions = array();
        
        foreach($result as $r){
            $transactions[] = Transaction::allParam($r);
        }

        return $transactions;
    }

    public static function findByTargetUserId($id) {
        $connection = DBConnection::getInstance()->getConnection();
        
        $stmt = $connection->prepare(self::FIND_BY_TARGET_USER_ID);     
        $stmt->bindParam(':target_user_id', $id);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $transactions = array();

        foreach($result as $r){
            $transactions[] = Transaction::allParam($r);
        }

        return $transactions;
    }

    public static function update($id, Transaction $transaction) {
        $connection = DBConnection::getInstance()->getConnection();

        $currency_amount = $transaction->getCurrencyAmount();
        $currency_type = $transaction->getCurrencyType();
        $source_user_id = $transaction->getSourceUserId();
        $target_user_id = $transaction->getTargetUserId();
        $created_timestamp = $transaction->getCreatedTimestamp();
        $processed_timestamp = $transaction->getProcessedTimestamp();
        $state = $transaction->getState();

        $stmt = $connection->prepare(self::UPDATE);
        $stmt->bindParam(':currency_amount', $currency_amount);
        $stmt->bindParam(':currency_type', $currency_type);
        $stmt->bindParam(':source_user_id', $source_user_id);
        $stmt->bindParam(':target_user_id', $target_user_id);
        $stmt->bindParam(':created_timestamp', $created_timestamp);
        $stmt->bindParam(':processed_timestamp', $processed_timestamp);
        $stmt->bindParam(':state', $state);

        $stmt->bindParam(':id', $id);

        $stmt->execute();

        return $id;
    }
}