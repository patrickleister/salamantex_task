<?php

namespace model\http;

class HttpStatus
{
    public const OK = 200;
    public const CREATED = 201;
    public const ACCEPTED = 202;
    public const NOT_MODIFIED = 304;
    public const NOT_FOUND = 404;
    public const BAD_REQUEST = 400;
    public const INTERNAL_ERROR = 500;
    public const NOT_ACCEPTABLE = 406;
    public const UNSUPPORTED_MEDIA_TYPE = 415;
    public const NO_CONTENT = 204;
}