<?php

namespace model\http;

class HttpMethod
{
    public const GET = 1;
    public const PUT = 2;
    public const DELETE = 3;
    public const POST = 4;
    public const PATCH = 5;
    public const HEAD = 6;
    public const CONNECT = 7;
    public const TRACE = 8;
    public const OPTIONS = 9;
}