<?php

namespace model\http;

class HttpResponse
{
    public static function getMessage($response, $status_code, $data){

        $response->code($status_code);
        $response->header('content-type', 'application/json', ';charset=utf-8');
        $response->body($data);

        return $response;
    }
}