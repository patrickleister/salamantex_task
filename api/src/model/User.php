<?php
  
  namespace model;
  require_once __DIR__ . '/../../vendor/autoload.php';

  use PHPUnit\Runner\Exception;

  class User {

    private $id;
    private $name;
    private $description;
    private $email;
    private $btcWalletId;
    private $btcWalletBalance;
    private $ethWalletId;
    private $ethWalletBalance;
    private $maxTransactionAmount;

    public function setId($value){ $this->id=$value; }
    public function getId(){ return $this->id; }

    public function setName($value) { $this->name = $value; }
    public function getName() { return $this->name; }

    public function setDescription($value) { $this->description = $value; }
    public function getDescription() { return $this->description; }

    public function setEmail($value) { $this->email = $value; }
    public function getEmail() { return $this->email; }
   
    public function setBtcWalletId($value) { $this->btcWalletId = $value; }
    public function getBtcWalletId() { return $this->btcWalletId; }

    public function setBtcWalletBalance($value) { $this->btcWalletBalance = $value; }
    public function getBtcWalletBalance() { return $this->btcWalletBalance; }

    public function setEthWalletId($value) { $this->ethWalletId = $value; }
    public function getEthWalletId() { return $this->ethWalletId; }

    public function setEthWalletBalance($value) { $this->ethWalletBalance = $value; }
    public function getEthWalletBalance() { return $this->ethWalletBalance; }

    public function setMaxTransactionAmount($value) { $this->maxTransactionAmount = $value; }
    public function getMaxTransactionAmount() { return $this->maxTransactionAmount; }


    public function __construct(){ }

    public static function normal($data) {
      $instance = new self();

      if(is_array($data)){
        $instance->setName($data['name']);
        $instance->setDescription($data['description']);
        $instance->setEmail($data['email']);
      }

      return $instance;
    }

    public static function allParam($data){
      $instance = new self();

      if(is_array($data)){
        $instance->setId($data['id']);
        $instance->setName($data['name']);
        $instance->setDescription($data['description']);
        $instance->setEmail($data['email']);
        $instance->setBtcWalletId($data['btc_wallet_id']);
        $instance->setBtcWalletBalance($data['btc_wallet_balance']);
        $instance->setEthWalletId($data['eth_wallet_id']);
        $instance->setEthWalletBalance($data['eth_wallet_balance']);
        $instance->setMaxTransactionAmount($data["max_transaction_amount"]);
      }
      return $instance;
    }

    public function toString(){
        return  "id : ".$this->getId()."<br>".
                "name : ".$this->getName()."<br>".
                "description : ".$this->getDescription()."<br>".
                "email : ".$this->getEmail()."<br>".
                "btc-wallet-id : ".$this->getBtcWalletId()."<br>".
                "btc-wallet-balance : ".$this->getBtcWalletBalance()."<br>".
                "eth-wallet-id : ".$this->getEthWalletId()."<br>".
                "eth-wallet-balance : ".$this->getEthWalletBalance()."<br>".
                "max-transaction-amount : ".$this->getMaxTransactionAmount();
    }

    public function json(){
      return  '{
                "id" : "'.$this->getId().'",'.
                '"name" : "'.$this->getName().'",'.
                '"description" : "'.$this->getDescription().'",'.
                '"email" : "'.$this->getEmail().'",'.
                '"btc_wallet_id" : "'.$this->getBtcWalletId().'",'.
                '"btc_wallet_balance" : "'.$this->getBtcWalletBalance().'",'.
                '"eth_wallet_id" : "'.$this->getEthWalletId().'",'.
                '"eth_wallet_balance" : "'.$this->getEthWalletBalance().'",'.
                '"max_transaction_amount" : "'.$this->getMaxTransactionAmount().'"
              }';
    }

    public function shortJson() {
      return  '{
                "id" : "'.$this->getId().'",'.
                '"name" : "'.$this->getName().'",'.
                '"email" : "'.$this->getEmail().'"
              }';
    }
}
 