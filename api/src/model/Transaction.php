<?php
  
  namespace model;
  require_once __DIR__ . '/../../vendor/autoload.php';

  use PHPUnit\Runner\Exception;

  class Transaction {

    private $id;
    private $currencyAmount;
    private $currencyType;
    private $sourceUserId;
    private $targetUserId;
    private $createdTimestamp;
    private $processedTimestamp;
    private $state;

    public function setId($value){ $this->id=$value; }
    public function getId(){ return $this->id; }

    public function setCurrencyAmount($value) { $this->currencyAmount = $value; }
    public function getCurrencyAmount() { return $this->currencyAmount; }

    public function setCurrencyType($value) { $this->currencyType = $value; }
    public function getCurrencyType() { return $this->currencyType; }

    public function setSourceUserId($value) { $this->sourceUserId = $value; }
    public function getSourceUserId() { return $this->sourceUserId; }
   
    public function setTargetUserId($value) { $this->targetUserId = $value; }
    public function getTargetUserId() { return $this->targetUserId; }

    public function setCreatedTimestamp($value) { $this->createdTimestamp = $value; }
    public function getCreatedTimestamp() { return $this->createdTimestamp; }

    public function setProcessedTimestamp($value) { $this->processedTimestamp = $value; }
    public function getProcessedTimestamp() { return $this->processedTimestamp; }

    public function setState($value) { $this->state = $value; }
    public function getState() { return $this->state; }

    public function __construct(){
      
    }

    public static function normal($data){
      $instance = new self();
      
      if(is_array($data)){
        $instance->setCurrencyAmount($data['currency_amount']);
        $instance->setCurrencyType($data['currency_type']);
        $instance->setSourceUserId($data['source_user_id']);
        $instance->setTargetUserId($data['target_user_id']);
        $instance->setState($data['state']);
      }
      return $instance;
    }

    public static function allParam($data){
      $instance = new self();
      
      if(is_array($data)){
        $instance->setId($data['id']);
        $instance->setCurrencyAmount($data['currency_amount']);
        $instance->setCurrencyType($data['currency_type']);
        $instance->setSourceUserId($data['source_user_id']);
        $instance->setTargetUserId($data['target_user_id']);
        $instance->setCreatedTimestamp($data['created_timestamp']);
        $instance->setProcessedTimestamp($data['processed_timestamp']);
        $instance->setState($data['state']);
      }
      return $instance;
    }

    public function toString(){
        return  "id : ".$this->getId()."<br>".
                "currency_amount : ".$this->getCurrencyAmount()."<br>".
                "currency_type : ".$this->getCurrencyType()."<br>".
                "source_user_id : ".$this->getSourceUserId()."<br>".
                "target_user_id : ".$this->getTargetUserId()."<br>".
                "created_timestamp : ".$this->getCreatedTimestamp()."<br>".
                "processed_timestamp : ".$this->getProcessedTimestamp()."<br>".
                "state : ".$this->getState();
    }
}
 