<?php
namespace routes;
require_once __DIR__ . '/../../vendor/autoload.php';

use Klein\Klein;
use Klein\Request;
use Klein\Response;

class Routing {

    public static function route(){

        $base  = dirname($_SERVER['PHP_SELF']);

        //Bereitet die Anfragen für die Klein-PHP Bibliothek vor
        if(ltrim($base, '/')){
            $_SERVER['REQUEST_URI'] = substr($_SERVER['REQUEST_URI'], strlen($base));
        }

        $klein = new Klein();

        $klein->with('/user', __DIR__ . '/UserRoutes.php');
        $klein->with('/transaction', __DIR__ . '/TransactionRoutes.php');
        $klein->dispatch();

    }
}