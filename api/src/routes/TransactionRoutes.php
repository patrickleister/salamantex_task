<?php

namespace routes;
require_once __DIR__ . '/../../vendor/autoload.php';

use Klein\Klein;
use Klein\Request;
use Klein\Response;
use controller\TransactionController;

// get transaction by id 
$this->respond('GET', '/?', function (Request $request, Response $response) {
    return TransactionController::getTransaction($request, $response);
});

// submit transaction to the system
$this->respond('POST', '/?', function (Request $request, Response $response) {
    return TransactionController::createTransaction($request, $response);
});

// get transaction state by id
$this->respond('GET', '/[:id]/state', function (Request $request, Response $response) {
    return TransactionController::getTransactionState($request, $response);
});

// get all pending transactions
$this->respond('GET', '/pending', function (Request $request, Response $response){
    return TransactionController::getPendingTransactions($request, $response);
}); 

// update transaction
$this->respond('PUT', '/?', function(Request $request, Response $response){
    return TransactionController::update($request, $response);
});