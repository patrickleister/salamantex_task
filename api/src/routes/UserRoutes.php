<?php

namespace routes;
require_once __DIR__ . '/../../vendor/autoload.php';

use Klein\Klein;
use Klein\Request;
use Klein\Response;
use controller\UserController;

// get user by id
$this->respond('GET', '/?', function (Request $request, Response $response) {
    return UserController::get($request, $response);
});

// create user 
$this->respond('POST', '/?', function (Request $request, Response $response) {
    return UserController::create($request, $response);
});

// add currency account
$this->respond('POST', '/[:id]/currAcc', function (Request $request, Response $response) {
    return UserController::addTransactionAccount($request, $response);
});

// get transaction history by user
$this->respond('GET', '/[:id]/transactionHistory', function (Request $request, Response $response) {
    return UserController::getTransactionHistoryByUser($request, $response);
});

// update user
$this->respond('PUT', '/?', function(Request $request, Response $response) {
    return UserController::update($request, $response);
});

